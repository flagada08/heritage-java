package com.terence.exe1;

import javax.swing.JOptionPane;

/**
 * @author User-05
 *
 */
public class Employe {
	private String nom;
	private int age;
	protected double salaire;
	private double augmentation;
	private double nouveauSalaire;
	
	/**
	 * @param nom
	 * @param age
	 * @param salaire
	 * @param augmentation
	 */
	public Employe(String nom, int age, double salaire, int augmentation) {
		super();
		this.nom = nom;
		this.age = age;
		this.salaire = salaire;
		this.augmentation = augmentation;
	}
	/**
	 * @param pAugmentation
	 * @return
	 */
	public double Augmentation(double pAugmentation) {
		this.augmentation = pAugmentation;
			return Math.round(100.0/salaire * pAugmentation);
	}
	@Override
	public String toString() {
		return "Employe [nom : " 
			+ nom 
			+ ", age : " 
			+ age 
			+ ", salaire : " 
			+ calculeSalaire() 
			+ " BTC" 
			+ "]\n" 
			+ "[augmentation de : " 
			+ augmentation 
			+ " BTC]\n" 
			+ "[soit : " 
			+ Augmentation(augmentation) 
			+ "%]";
	}
	/**
	 * @param pEmploye
	 */
	public void afficher(Employe pEmploye) {
		JOptionPane.showMessageDialog(null, pEmploye.toString(), nom, JOptionPane.INFORMATION_MESSAGE);
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the salaire
	 */
	public double getSalaire() {
		return salaire;
	}
	/**
	 * @param salaire the salaire to set
	 */
	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}
	/**
	 * @return
	 */
	public double calculeSalaire() {
		return salaire;
	}
}
