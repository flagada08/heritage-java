package com.terence.exe1;

/**
 * @author User-05
 *
 */
public class Technicien extends Employe {
	private GradeTechnicien grade;
	
	/**
	 * @param nom
	 * @param age
	 * @param salaire
	 * @param pGrade
	 * @param pAugmentation
	 */
	public Technicien(String nom, int age, double salaire, GradeTechnicien pGrade, int pAugmentation) {
		super(nom, age, salaire, pAugmentation);
		grade = pGrade;
	}
	
	/**
	 * @param pSalaire
	 * @return
	 */
	private double prime(double pSalaire) {
		salaire = pSalaire;
		if(grade == GradeTechnicien.GRADE1) {
			pSalaire += 100;
		} else if(grade == GradeTechnicien.GRADE2) {
			pSalaire += 200;
		} else if(grade == GradeTechnicien.GRADE3) {
			pSalaire += 300;
		}
		return pSalaire;
	}
	
//	@Override
//	public double Augmentation(int pAugmentation) {
//		// TODO Auto-generated method stub
//		return super.Augmentation(pAugmentation);
//	}
	
	@Override
	public String toString() {
		return "Technicien [" + grade + "]\n" + super.toString();
	}

	/**
	 *
	 */
	@Override
	public double calculeSalaire() {
		// TODO Auto-generated method stub
		return prime(salaire);
	}

	/**
	 * @return the grade
	 */
	public GradeTechnicien getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(GradeTechnicien grade) {
		this.grade = grade;
	}
	
}
