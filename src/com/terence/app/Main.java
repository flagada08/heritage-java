package com.terence.app;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.swing.JOptionPane;

import com.terence.exe1.Employe;
import com.terence.exe1.GradeTechnicien;
import com.terence.exe1.Technicien;
import com.terence.exe2.Animal;
import com.terence.exe2.Chien;
import com.terence.exe2.Oiseau;
import com.terence.exe3.Question;
import com.terence.exe3.QuestionReponse;
import com.terence.exe4.ActesMedicaux;
import com.terence.exe4.Maladie;
import com.terence.exe4.Patient;
import com.terence.exe4.Soignant;
import com.terence.exe4.Soin;
import com.terence.exe5.Assistant;
import com.terence.exe5.Elu;
import com.terence.exe5.TypElu;

/**
 * @author User-05
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/**
		 * HERITAGE com.terence.exe1
		 */
//		Employe empTerence = new Employe("TERENCE", 38, 3000, 100);
//		empTerence.afficher(empTerence);
//
//		Technicien techTerence = new Technicien("TERENCE", 38, 3000, GradeTechnicien.GRADE3, 500);
//		techTerence.afficher(techTerence);
//
//		Employe empEhsan = new Employe("EHSAN", 29, 2000, 200);
//		empEhsan.afficher(empEhsan);
//
//		Technicien techEhsan = new Technicien("EHSAN", 29, 2000, GradeTechnicien.GRADE2, 600);
//		techEhsan.afficher(techEhsan);
		
		/**
		 * HERITAGE com.terence.exe2
		 */
//		Animal naya = new Chien("Naya", "Chien", 9, "Staff");
//		JOptionPane.showMessageDialog(null, naya, "Naya", 1);
//		naya.manger("des croquettes");
//		naya.dormir();
//		Chien chien = new Chien("Naya", "Chien", 9, "Staff");
//		chien.aboyer();
//		Oiseau titi = new Oiseau("Titi", "Canari", 3, 213546);
//		JOptionPane.showMessageDialog(null, titi, "Titi", 1);
//		titi.manger("des graines");
//		titi.dormir();
//		titi.senvoler();
		
		/**
		 *  HERITAGE com.terence.exe3
		 */
//		QuestionReponse QCM = new QuestionReponse();
//		QCM.commencer();
//		
		/**
		 *  HERITAGE com.terence.exe4
		 */
//		Personne p1 = new Personne("NOM_P1", "PRENOM_P1");
//		System.out.println(p1);
		
//		Date dateNaissance = new Date();
//		Instant nowInstant = Clock.systemDefaultZone().instant();
//		Instant dateDiagnostic = Instant.now();
//		Date dateSoinPatient = new Date();
//		
//		Soin soin = new Soin(
//			"DOLIPRANE", 
//				15, 
//				dateSoinPatient, 
//				50
//			);
//		
//		ActesMedicaux actMedicaux = 
//			new ActesMedicaux(
//				soin, 
//				dateDiagnostic, 
//				Maladie.GRIPPE, 
//				75
//			);
//		
//		Soignant soignant = 
//			new Soignant(
//				"NOM_SOIGNANT", 
//				"PRENOM_SOIGNANT", 
//				actMedicaux
//			);
//
//		new Patient("NOM_PATIENT", "PRENOM_PATIENT", dateNaissance, "HOMME", soin, soignant).afficher();
		
		/**
		 *  HERITAGE com.terence.exe5
		 */
		Assistant a1 = new Assistant("nAssistant1", "npAssistant1", 1500);
		Elu e1 = new Elu("De Gaulle", "Charles", 500000, a1, TypElu.NORMAL, null);
		e1.addAssistant(a1);
		a1.addSous(1500);
		System.out.println(e1);
	}

}
