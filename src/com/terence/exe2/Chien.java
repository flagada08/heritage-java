package com.terence.exe2;

import javax.swing.JOptionPane;

/**
 * @author User-05
 *
 */
public class Chien extends Animal {
	String race;
	
	/**
	 * @param nom
	 * @param espece
	 * @param age
	 * @param pRace
	 */
	public Chien(String nom, String espece, int age, String pRace) {
		super(nom, espece, age);
		race = pRace;
	}

	public void aboyer() {
		JOptionPane.showMessageDialog(null, "J'aboie", nom, 1);
	}
	@Override
	public void dormir() {
		JOptionPane.showMessageDialog(null, "Je dors dans une niche", nom, 1);		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " et je suis un " + race;
	}

	/**
	 * @return the race
	 */
	public String getRace() {
		return race;
	}

	/**
	 * @param race the race to set
	 */
	public void setRace(String race) {
		this.race = race;
	}
	
}
