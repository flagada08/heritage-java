package com.terence.exe2;

import javax.swing.JOptionPane;

/**
 * @author User-05
 *
 */
public abstract class Animal {
	String nom;
	String espece;
	int age;
	
	/**
	 * @param nom
	 * @param espece
	 * @param age
	 */
	public Animal(String nom, String espece, int age) {
		this.nom = nom;
		this.espece = espece;
		this.age = age;
	}
	/**
	 * @param pNourriture
	 */
	public void manger(String pNourriture) {
		JOptionPane.showMessageDialog(null, "Je peux manger " + pNourriture, nom, 1);
	}
	/**
	 * 
	 */
	public abstract void dormir();
	
	@Override
	public String toString() {
		return "Je m'appelle " + nom + ", je suis un " + espece + ", j'ai " + age + " ans";
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the espece
	 */
	public String getEspece() {
		return espece;
	}
	/**
	 * @param espece the espece to set
	 */
	public void setEspece(String espece) {
		this.espece = espece;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
}
