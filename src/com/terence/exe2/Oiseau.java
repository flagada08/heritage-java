package com.terence.exe2;

import javax.swing.JOptionPane;

/**
 * @author User-05
 *
 */
public class Oiseau extends Animal{
	int nombreDePlumes;

	/**
	 * @param nom
	 * @param espece
	 * @param age
	 */
	public Oiseau(String nom, String espece, int age, int pNombreDePlumes) {
		super(nom, espece, age);
		nombreDePlumes = pNombreDePlumes;
	}

	public void senvoler() {
		JOptionPane.showMessageDialog(null, "Je vole", nom, 1);
	}
	
	public void manger(String nourriture) {
		JOptionPane.showMessageDialog(null, "Je ne mange que " + nourriture, nom, 1);
	}
	
	@Override
	public void dormir() {
		JOptionPane.showMessageDialog(null, "Je dors dans un nid", nom, 1);		
	}

	@Override
	public String toString() {
		return super.toString() + " et j'ai " + nombreDePlumes + " plumes";
	}

	/**
	 * @return the nimbreDePlumes
	 */
	public int getNombreDePlumes() {
		return nombreDePlumes;
	}

	/**
	 * @param nimbreDePlumes the nimbreDePlumes to set
	 */
	public void setNombreDePlumes(int nimbreDePlumes) {
		this.nombreDePlumes = nimbreDePlumes;
	}
	
}
