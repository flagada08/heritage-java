package com.terence.exe3;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionReponse {
	private ArrayList<Question> questionReponseArray;
	/**
	 * @param questionReponseArray
	 */
	public QuestionReponse() {
		super();
		this.questionReponseArray = new ArrayList<Question>();
		String q1 = "Java est = à JavaScript ?";
		String q2 = "PHP = langage compilé ?";
		String[] r1 = {"Vrai, Java == JavaScript", "Faux, Java != JavaScript", "C'est pas faux"};
		String[] r2 = {"Vrai, PHP == Compilé", "Faux, PHP != Compilé", "C'est pas faux"};
		questionReponseArray.add(new Question(q1, r1, "Faux, Java != JavaScript", 1));
		questionReponseArray.add(new Question(q2, r2, "Faux, PHP != Compilé", 3));
	}
	public void commencer() {
		Scanner scanner = new Scanner(System.in);
		int nCorrect = 0;
		// affichage des questions de l'arrayList questionReponseArray
		for(int q = 0; q < questionReponseArray.size(); q++) {
			System.out.println(questionReponseArray.get(q).getEnonce());
			int nChoix = questionReponseArray.get(q).getChoix().size();
			// affichage du choix des réponses liées aux questions dans questionReponseArray
			for(int c = 0; c < nChoix; c++) {
				System.out.println((c + 1) + ": " + questionReponseArray.get(q).getChoix().get(c));
			}
			System.out.println("\nVeuillez entrer votre choix : ");
			int userReponse = scanner.nextInt();
			ArrayList<String> sChoix = questionReponseArray.get(q).getChoix();
			String cReponse = questionReponseArray.get(q).getReponse();
			int cReponseIndex = sChoix.indexOf(cReponse);
			if(userReponse == cReponseIndex +1) {
				nCorrect++;
			}
		}
		scanner.close();
		System.out.println("Vous avez obtenu " + nCorrect + " réponse(s) correcte(s)");
	}
}
