package com.terence.exe3;

import java.util.ArrayList;

/**
 * @author User-05
 *
 */
public class Question {
	private String enonce;
	private ArrayList<String> choix;
	private String reponse;
	private int difficulte = 50; // la difficulté varie de 0 à 100
	
	public Question(String pEnonce, String[] pChoix, String pReponse, int pDifficulte) {
		this.enonce = pEnonce;
		this.choix = new ArrayList<String>();
		for(int i = 0; i < pChoix.length; i++) {
			this.choix.add(pChoix[i]);
		}
		this.reponse = pReponse;
		this.difficulte = pDifficulte;
	}

	public String getEnonce() {
		return enonce;
	}

	public ArrayList<String> getChoix() {
		return choix;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	
	public int getDifficulte() {
		return difficulte;
	}

}
