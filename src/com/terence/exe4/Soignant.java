package com.terence.exe4;

public class Soignant extends Personne{
	private ActesMedicaux actesMedicaux;
	
	/**
	 * @param nom
	 * @param prenom
	 * @param soin
	 */
	public Soignant(String nom, String prenom, ActesMedicaux actesMedicaux) {
		super(nom, prenom);
		this.actesMedicaux = actesMedicaux;
	}
	
	@Override
	public String toString() {
		return "Soignant :\n  nom : " + getNom() 
				+ ", prenom : " + getPrenom() 
				+ "\n"
				+ getActesMedicaux();
	}
	
	public ActesMedicaux getActesMedicaux() {
		return actesMedicaux;
	}

	public void setActesMedicaux(ActesMedicaux actesMedicaux) {
		this.actesMedicaux = actesMedicaux;
	}
}
