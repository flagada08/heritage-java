package com.terence.exe4;

public class Medicament {
	private String nomMedicament;
	private int medicament;
	
	/**
	 * @param nomMedicament
	 * @param medicament
	 */
	public Medicament(String nomMedicament, int medicament) {
		super();
		this.medicament = medicament;
		this.nomMedicament = nomMedicament;
	}

	@Override
	public String toString() {
		return "\n\nMedicament prescrit : " + getNomMedicament() 
				+ "\nNombre de médicament : " + getMedicament() + "/jour";
	}

	/**
	 * @return the medicament
	 */
	public int getMedicament() {
		return medicament;
	}

	/**
	 * @param medicament the medicament to set
	 */
	public void setMedicament(int medicament) {
		this.medicament = medicament;
	}

	/**
	 * @return the nomMedicament
	 */
	public String getNomMedicament() {
		return nomMedicament;
	}

	/**
	 * @param nomMedicament the nomMedicament to set
	 */
	public void setNomMedicament(String nomMedicament) {
		this.nomMedicament = nomMedicament;
	}

}
