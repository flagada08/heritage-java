package com.terence.exe4;

import java.util.Date;

public class Soin extends Medicament{
	private Date dateSoin = new Date();
	private int etatPatient;
	
	/**
	 * @param dateSoin
	 * @param etatPatient
	 */
	public Soin(String nomMedicament, int medicament, Date dateSoin, int etatPatient) {
		super(nomMedicament, medicament);
		this.dateSoin = dateSoin;
		this.etatPatient = etatPatient; 
	}
	@Override
	public String toString() {
		return "\n Soin : date : " + getDateSoin() + "," 
				+ " etat patient : " + getEtatPatient() + "%\n"
				+ "\nMedicament prescrit : " + getNomMedicament() 
				+ "\nNombre de médicament : " + getMedicament() + "/Jour";
	}
	/**
	 * @return the dateSoin
	 */
	public Date getDateSoin() {
		return dateSoin;
	}
	/**
	 * @param dateSoin the dateSoin to set
	 */
	public void setDateSoin(Date dateSoin) {
		this.dateSoin = dateSoin;
	}
	/**
	 * @return the etatPatient
	 */
	public int getEtatPatient() {
		return etatPatient;
	}
	/**
	 * @param etatPatient the etatPatient to set
	 */
	public void setEtatPatient(int etatPatient) {
		this.etatPatient = etatPatient;
	}
}
