package com.terence.exe4;

import java.time.Instant;

public class ActesMedicaux extends Diagnostic {
	private Soin soin;
	/**
	 * @param soin
	 * @param soignant
	 * @param date
	 * @param maladie
	 * @param validite
	 */
	public ActesMedicaux(Soin soin, Instant date, Maladie maladie, int validite) {
		super(soin, date, maladie, validite);
		this.soin = soin;
	}
	
	@Override
	public String toString() {
		return "Actes Medicaux :\n" 
		+ " Diagnostic : " + getMaladie() 
		+ getSoin();
	}

	public Soin getSoin() {
		return soin;
	}

	public void setSoin(Soin soin) {
		this.soin = soin;
	}
}
