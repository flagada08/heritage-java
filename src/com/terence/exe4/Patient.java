package com.terence.exe4;

import java.util.Date;

import javax.swing.JOptionPane;

public class Patient extends Personne {
	private Date dateDeNaissance;
	private String sexe;	
	private Soin soin;
	private Soignant soignant;
	
	/**
	 * @param nom
	 * @param prenom
	 * @param dateDeNaissance
	 * @param sexe
	 * @param actesMedicauxDiag
	 */
	public Patient(String nom, String prenom, Date dateDeNaissance, String sexe, Soin soin, Soignant soignant) {
		super(nom, prenom);
		this.dateDeNaissance = dateDeNaissance;
		this.sexe = sexe;
		this.setSoin(soin);
		this.soignant = soignant;
	}

	@Override
	public String toString() {
		return "Identité du patient :\n  nom : " + getNom() 
				+ ", prenom : " + getPrenom() 
				+ "\n  date de naissance : " + getDateDeNaissance() 
				+ "\n  sexe : " + getSexe()
				+ "\n" + getSoignant();
	}
	
	public void afficher() {
		Patient patient = 
			new Patient(
				getNom(), 
				getPrenom(), 
				getDateDeNaissance(), 
				getSexe(), 
				getSoin(), 
				getSoignant()
			);
		JOptionPane.showMessageDialog(null, patient);
	}

	/**
	 * @return the dateDeNaissance
	 */
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	/**
	 * @param dateDeNaissance the dateDeNaissance to set
	 */
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	/**
	 * @return the sexe
	 */
	public String getSexe() {
		return sexe;
	}

	/**
	 * @param sexe the sexe to set
	 */
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public Soignant getSoignant() {
		return soignant;
	}

	public void setSoignant(Soignant soignant) {
		this.soignant = soignant;
	}

	public Soin getSoin() {
		return soin;
	}

	public void setSoin(Soin soin) {
		this.soin = soin;
	}

}
