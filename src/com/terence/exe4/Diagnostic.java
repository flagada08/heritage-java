package com.terence.exe4;

import java.time.Instant;

public class Diagnostic {
	private Soignant soignant;
	private Instant date;
	private Maladie maladie;
	private int validite; // fiabilité du diagnostic, entre 0 et 100%
	
	/**
	 * @param date
	 * @param maladie
	 * @param validite
	 */
	public Diagnostic(Medicament medicament, Instant date, Maladie maladie, int validite) {
		super();
		this.date = date;
		this.maladie = maladie;
		this.validite = validite;
	}

	@Override
	public String toString() {
		return "\n\nSoin prodigué et Diagnostic établi par le " + getSoignant() 
				+ "\n  Diagnostic : date diagnostic : " + getDate() 
				+ ", maladie : " + getMaladie() 
				+ ", validite : " + getValidite() + "%";
	}

	/**
	 * @return the date
	 */
	public Instant getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Instant date) {
		this.date = date;
	}

	/**
	 * @return the maladie
	 */
	public Maladie getMaladie() {
		return maladie;
	}

	/**
	 * @param maladie the maladie to set
	 */
	public void setMaladie(Maladie maladie) {
		this.maladie = maladie;
	}

	/**
	 * @return the validite
	 */
	public int getValidite() {
		return validite;
	}

	/**
	 * @param validite the validite to set
	 */
	public void setValidite(int validite) {
		this.validite = validite;
	}

	/**
	 * @return the soignant
	 */
	public Soignant getSoignant() {
		return soignant;
	}

	/**
	 * @param soignant the soignant to set
	 */
	public void setSoignant(Soignant soignant) {
		this.soignant = soignant;
	}
}
