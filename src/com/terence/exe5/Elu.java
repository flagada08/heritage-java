package com.terence.exe5;

import java.util.ArrayList;

public class Elu extends Personne {
	private Assistant assistant;
	private TypElu typelu;
	private ArrayList<Assistant> assistants;

	/**
	 * @param nom
	 * @param prenom
	 * @param compteBancaire
	 * @param assistant
	 * @param typelu
	 * @param assistants
	 */
	public Elu(String nom, String prenom, int compteBancaire, Assistant assistant, TypElu typelu,
			Assistant[] assistants) {
		super(nom, prenom, compteBancaire);
		this.assistant = assistant;
		this.typelu = typelu;
		this.assistants = new ArrayList<Assistant>();
		for(int i = 0; i < assistants.length; i++) {
			this.assistants.add(assistants[i]);
		};
	}

	@Override
	public String toString() {
		return super.toString() + "\nElu [assistant=" + assistant + ", typelu=" + typelu + ", assistants=" + assistants + "]";
	}
	
	public void addAssistant(Assistant assistant) {
		this.setAssistant(new Assistant(getNom(), getPrenom(), getCompteBancaire()));
		this.assistants = new ArrayList<Assistant>();
		assistants.add(assistant);
	}

	public void deleteAssistant() {
		assistants.remove(getAssistant());
	}
	
	public void verserDotation() {
		
	}

	/**
	 * @return the assistant
	 */
	public Assistant getAssistant() {
		return assistant;
	}

	/**
	 * @param assistant the assistant to set
	 */
	public void setAssistant(Assistant assistant) {
		this.assistant = assistant;
	}

	/**
	 * @return the typelu
	 */
	public TypElu getTypelu() {
		return typelu;
	}

	/**
	 * @param typelu the typelu to set
	 */
	public void setTypelu(TypElu typelu) {
		this.typelu = typelu;
	}

	/**
	 * @return the assistants
	 */
	public ArrayList<Assistant> getAssistants() {
		return assistants;
	}

	/**
	 * @param assistants the assistants to set
	 */
	public void setAssistants(ArrayList<Assistant> assistants) {
		this.assistants = assistants;
	}
}
