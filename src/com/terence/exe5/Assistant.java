package com.terence.exe5;

import java.util.ArrayList;

public class Assistant extends Personne{
	private ArrayList<String> assistantDunElu;

	public Assistant(String nom, String prenom, int compteBancaire) {
		super(nom, prenom, compteBancaire);
		
	}

	/**
	 * @return the assistantDunElu
	 */
	public ArrayList<String> getAssistantDunElu() {
		return assistantDunElu;
	}

	/**
	 * @param assistantDunElu the assistantDunElu to set
	 */
	public void setAssistantDunElu(ArrayList<String> assistantDunElu) {
		this.assistantDunElu = assistantDunElu;
	}

}
