package com.terence.exe5;

public class Personne {
	private String nom, prenom;
	private int compteBancaire; // montant en euros
	/**
	 * @param nom
	 * @param prenom
	 * @param compteBancaire
	 */
	public Personne(String nom, String prenom, int compteBancaire) {
		super();
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setCompteBancaire(compteBancaire);
	}
	public void addSous(int montant) {
		this.setCompteBancaire(this.getCompteBancaire() + montant);
	}
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", compteBancaire=" + compteBancaire + "]";
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the compteBancaire
	 */
	public int getCompteBancaire() {
		return compteBancaire;
	}
	/**
	 * @param compteBancaire the compteBancaire to set
	 */
	public void setCompteBancaire(int compteBancaire) {
		this.compteBancaire = compteBancaire;
	}
	
}
